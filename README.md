# Monte Carlo Inference Toolbox  #

Please see README.m

### Published Papers ###

* B. Sengupta, K.J. Friston and W. Penny “Gradient-based MCMC samplers for Dynamic Causal Modelling” (In Press, Neuroimage)
* B. Sengupta, K.J. Friston and W.Penny “Gradient-free MCMC for Dynamic Causal Modelling” Neuroimage S1053-8119(15)00184-6 (2015)
* B. Sengupta, K.J. Friston and W. Penny “Efficient gradient computations for dynamical models” Neuroimage S1053-8119(14)00309-7 (2014)