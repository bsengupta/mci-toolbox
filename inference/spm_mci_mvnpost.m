function [stats,k,f,Y,X] = spm_mci_mvnpost (post,method,verbose,max_lag)
% Are MCMC samples consistent with Gaussian posterior ?
% FORMAT [stats,k,f,Y,X] = spm_mci_mvnpost (post,method,verbose,max_lag)
%
% post      posterior data structure from spm_mci_post
% method    'ESS' or 'MAR'
% verbose   create plots
% max_lag   maximum potential lag for MAR model
% 
% stats     (multivariate) normal test statistics
%           See spm_mci_mvntest.m
% k         optimal MAR order
% f(p)      model evidence for order p MAR model
% Y         uncorrelated posterior samples
% X         original posterior samples
% 
% Fit an MAR(k) model to posterior samples and remove predictions
% Run Normality tests on resulting IID data
%__________________________________________________________________________
% Copyright (C) 2015 Wellcome Trust Centre for Neuroimaging

% Will Penny 
% $Id$

try pmax=max_lag; catch pmax=10; end
try meth=method; catch meth='ESS'; end
try ver=verbose; catch ver=1; end


j=post.ind;
X=post.P(:,j)';
Nj=length(j);
Np=size(post.P,1);
        
if any(std(X)==0)
    disp('Warning from spm_mci_mvnpost: some parameters have zero variance');
    stats=[];
    return
end

switch meth
    case 'ESS',
        %C=cov(post.P(:,post.ind)');
        for p=1:Np,
            ess(p)=spm_mci_ess(post.P(p,j));
            %v(p)=C(p,p)*ess(p)/Nj;
        end
        %post.Cp=diag(v);
        %mess=mean(ess);
        mess=ceil(min(ess));
        stats = spm_mci_mvntest(X,mess);
        
        if ver
            disp('ESS method');
            disp('Effective Sample Sizes:');
            disp(ess);
        end
        
    case 'thinning',
        for p=1:Np,
            [tmp,m(p)]=spm_mci_ess(post.P(p,j));
        end
        lag=max(m)+1;
        Y=X(1:lag:end,:);
        stats = spm_mci_mvntest(Y);
        if ver
            disp('Thinning method');
            disp(sprintf('Using only every %d-th sample',lag));
        end
        
    case 'MAR',
        m=mean(X);
        N=size(X,1);
        X=X-ones(N,1)*m;
        % Fit a range of MAR models
        for p=1:pmax,
            [mar,y,ypred]=spm_mar(X,p);
            f(p)=mar.fm;
            model(p).X=y-ypred;
        end
        
        % Find best MAR model
        [tmp,k]=max(f);
        Y=model(k).X;
        T=size(Y,1);
        Y=Y+ones(T,1)*m;
        X=X+ones(N,1)*m;
        
        stats = spm_mci_mvntest(X);
        
        if ver
            disp('MAR method');
            disp(sprintf('Optimal MAR order = %d',k));
            Np=size(X,2);
            rNp=ceil(sqrt(Np));
            h=figure;
            set(h,'Name','Posterior Samples');
            for j=1:Np,
                subplot(rNp,rNp,j);
                plot(X(:,j));
                hold on
                plot(Y(:,j),'k');
                grid on
                legend('Original','Decorrelated');
            end
            
            h=figure;
            set(h,'Name','Evidence for MAR(p) model');
            plot(f);
            xlabel('p');
            ylabel('Log Evidence');
            grid on
        end
        
    otherwise
        disp('Unknown method in spm_mci_mvnpost.m');
        return
end